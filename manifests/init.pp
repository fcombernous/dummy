# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   class { 'dummy' :
#     networks => {
#       'label' => {
#         macaddr => 'ae:ae:ae:ae:ae:ae',
#         address => '192.168.0.0/24',
#       },
#     },
#   }
#
# @param networks data used by systemd-networkd
#
class dummy (
  Dummy::Networks $networks = {},
) {

}
