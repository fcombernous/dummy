# A description of what this defined type does
#
# @summary A short summary of the purpose of this defined type.
#
# @example
#   dummy::network { 'namevar': }
define dummy::network(
  Stdlib::MAC $macaddr,
  Stdlib::IP::Address $address,
) {
}
