type Dummy::Network = Struct[{
  macaddr          => Stdlib::MAC,
  address          => Stdlib::IP::Address,
}]
